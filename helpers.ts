import { validateJwt } from "https://deno.land/x/djwt/validate.ts"
import { makeJwt, Jose } from "https://deno.land/x/djwt/create.ts"

const key = "pubox-dev-Sf3"

const header: Jose = {
  alg: "HS256",
  typ: "JWT",
}

interface GenerateToken {
  userId: string
}

interface VerifyToken {
  token: string
}

export const generateJwtToken = async ({ userId }: GenerateToken) => {
  try {
    if (!userId) {
      return Promise.reject('user_id was missing')
    }
    const payload = { user_id: userId }

    const access_token = await makeJwt({
      header,
      payload,
      key,
    })

    return {
      access_token
    }
  } catch (error) {
    throw Error(error)
  }
}

export const verifyJwtToken = async ({ token }: VerifyToken) => {
  try {
    if (!token) {
      return
    }

    const payload = await validateJwt(
      token,
      key,
    )

    return payload
  } catch (error) {
    console.log(error)
    throw Error(error)
  }
}
