import { Client } from "https://deno.land/x/mysql/mod.ts";

export const dbConnection = async () => {
  try {
    const client = await new Client().connect({
      hostname: "127.0.0.1",
      username: "root",
      db: "pubox",
      password: "",
    });

    return client
  } catch (error) {
    throw error
  }
}