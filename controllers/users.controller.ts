import { dbConnection } from '../db.connection.ts'
import { generateJwtToken, verifyJwtToken } from '../helpers.ts'

const client = await dbConnection()

export const login = async (context: any) => {
  try {
    const bodyRequest = await context.request.body()

    const { email, password } = bodyRequest.value

    if (!email || !password) {
      context.response.body = {
        status: 400,
        messageError: 'email or password was missing'
      }

      return
    }

    const [user] = await client.query(`select * from users where email = '${email}'`)
    
    if (!user || user.password !== password) {
      context.response.body = {
        status: 404,
        messageError: 'Email or password was wrong'
      }

      return
    }

    const tokenData = await generateJwtToken({
      userId: user.user_id
    })

    context.response.body = {
      status: 200,
      data: tokenData
    }
  } catch (error) {
    context.response.body = {
      status: 400,
      ...error
    }
  }
}

export const signup = async (context: any) => {
  try {
    const bodyRequest = await context.request.body()

    const { email, password, fullName } = bodyRequest.value
    
    if (!email || !password) {
      context.response.body = {
        status: 400,
        messageError: 'Email or password was missing'
      }
    }

    const [user] = await client.query(`select * from users where email = '${email}' `)

    if (!!user) {
      context.response.body = {
        status: 400,
        messageError: 'User is already exist'
      }

      return
    }

    await client.execute(`
      insert into users (email, password, full_name)
      values ('${email}', '${password}', '${fullName}')
    `)

    const [userData] = await client.query(`select * from users where email = '${email}'`)

    const tokenData = await generateJwtToken({
      userId: userData.user_id
    })

    context.response.body = {
      status: 200,
      data: tokenData
    }
  } catch (error) {
    context.response.body = {
      status: 500,
      messageError: 'Internal server'
    }
  }
}

export const validateAccessToken = async (context: any) => {
  try {
    const bodyRequest = await context.request.body()

    const { accessToken } = bodyRequest.value
    
    if (!accessToken) {
      context.response.body = {
        status: 401,
        messageError: 'Unauthorization'
      }

      return
    }

    const tokenPayload = await verifyJwtToken({
      token: accessToken
    })

    if (!tokenPayload || !tokenPayload.payload || !tokenPayload.payload.user_id) {
      context.response.body = {
        status: 400,
        messageError: 'Access token was valid'
      }

      return
    }

    const [userData] = await client.query(`select * from users where user_id = '${tokenPayload.payload.user_id}'`)

    if (!userData) {
      context.response.body = {
        status: 401,
        messageError: 'Unauthorization'
      }

      return
    }

    context.response.body = true
  } catch (error) {
    context.response.body = {
      status: 500,
      messageError: 'Internal server error'
    }
  }
}
