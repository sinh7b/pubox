import { dbConnection } from '../db.connection.ts'
import { verifyJwtToken } from '../helpers.ts'
import { v4 } from "https://deno.land/std/uuid/mod.ts";

const client = await dbConnection()

export const createMessage = async (context: any) => {
  try {
    const { roomId } = context.params
    const bodyRequest = await context.request.body()

    const { content, accessToken }: any = bodyRequest.value

    if (!accessToken) {
      context.response.body = {
        status: 400,
        message: 'Token is required',
      }
    }

    if (!content) {
      context.response.body = {
        status: 400,
        message: 'Content is required',
      }
    }


    const [tokenPayload, [room]] = await Promise.all([
      verifyJwtToken({
        token: accessToken
      }),
      client.query(`select * from rooms where room_id = '${roomId}'`)
    ])

    if (!room) {
      context.response.body = {
        status: 404,
        message: 'Room was not found',
      }

      return
    }

    if (!tokenPayload || !tokenPayload.payload || !tokenPayload.payload.user_id) {
      context.response.body = {
        status: 404,
        message: 'access token was invalid',
      }

      return
    }

    const primaryKey = v4.generate()

    await client.execute(`
    insert into messages (primary_key, content, created_by, room_id)
    values ('${primaryKey}', '${content}', ${tokenPayload.payload.user_id}, ${roomId})
    `)

    const [message] = await client.query(`
      select 
      m.message_id,
      m.created_at,
      m.content,
      JSON_OBJECT(
        'room_id', r.room_id,
        'created_at', r.created_at
      ) as room,
      JSON_OBJECT(
        'user_id', u.user_id,
        'email', u.email
      ) as user
      from messages as m
      left join users as u
      on u.user_id = m.created_by
      left join rooms as r
      on r.room_id = m.room_id
      where primary_key = '${primaryKey}'
    `)

    message.room = JSON.parse(message.room)
    message.user = JSON.parse(message.user)

    context.response.body = {
      status: 200,
      data: message,
    }
  } catch (error) {
    context.response.body = {
      status: 500,
      message: 'internal',
    }
  }
}

export const getMessages = async (context: any) => {
  try {
    const { roomId } = context.params

    let messages = await client.query(`
      select 
      m.message_id,
      m.created_at,
      m.content,
      JSON_OBJECT(
        'room_id', r.room_id,
        'created_at', r.created_at
      ) as room,
      JSON_OBJECT(
        'user_id', u.user_id,
        'email', u.email
      ) as user
      from messages as m
      left join users as u
      on u.user_id = m.created_by
      left join rooms as r
      on r.room_id = m.room_id
      where m.room_id = '${roomId}' order by m.message_id DESC
    `)

    messages = messages.map((each: any) => {
      each.room = JSON.parse(each.room)
      each.user = JSON.parse(each.user)

      return each
    })

    context.response.body = { messages }
  } catch (error) {
    throw error
  }
}
