import { Context } from "https://deno.land/x/abc@v1.0.0-rc2/mod.ts";
import { dbConnection } from '../db.connection.ts'
import { verifyJwtToken } from '../helpers.ts'
import { v4 } from "https://deno.land/std/uuid/mod.ts";

const client = await dbConnection()

export const createRoom = async (context: any) => {
  try {
    const bodyRequest = await context.request.body()

    const { email, accessToken }: any = bodyRequest.value

    if (!email) {
      context.response.body = {
        status: 400,
        messageError: 'Email is required'
      }

      return
    }

    if (!accessToken) {
      context.response.body = {
        status: 400,
        messageError: 'Access is required'
      }

      return
    }

    const tokenPayload = await verifyJwtToken({
      token: accessToken
    })

    if (!tokenPayload || !tokenPayload.payload || !tokenPayload.payload.user_id) {
      context.response.body = {
        status: 400,
        messageError: 'Access token was invalid'
      }
      
      return
    }

    const [[targetUser], rooms] = await Promise.all([
      client.query(`select * from users where email = '${email}'`),
      client.query(`select * from user_rooms where user_id = '${tokenPayload.payload.user_id}'`)
    ])

    if (!targetUser) {
      context.response.body = {
        status: 404,
        messageError: 'User was not found'
      }

      return
    }

    const roomIds = rooms.map((each: any) => each.room_id)

    let targetRoom

    if (roomIds.length > 0) {
      [targetRoom] = await client.query(`
        select 
        JSON_ARRAYAGG(JSON_OBJECT(
          'room_id', r.room_id,
          'created_at', r.created_at
        )) as rooms,
        JSON_ARRAYAGG(JSON_OBJECT(
          'user_id', u.user_id,
          'email', u.email
        )) as user
        from user_rooms as ur
        left join rooms as r on ur.room_id = r.room_id
        left join users as u on u.user_id = ur.user_id
        where ur.room_id in (${roomIds.join(', ')}) and ur.user_id = '${targetUser.user_id}'
        group by ur.room_id
      `)
    }

    if (targetRoom) {
      targetRoom.room = JSON.parse(targetRoom.rooms)[0]
      targetRoom.user = JSON.parse(targetRoom.user)

      delete targetRoom.rooms

      context.response.body = {
        data: targetRoom,
        status: 200,
      }

      return 
    }

    const uuid = v4.generate() 

    await client.execute(`
      insert into rooms (created_by, private_key) values ('${tokenPayload.payload.user_id}', '${uuid}')
    `)

    const [room] = await client.query(`
      select * from rooms where private_key = '${uuid}' and created_by = '1'
    `)

    await Promise.all([
      client.execute(
        `insert into user_rooms (room_id, user_id) values ('${room.room_id}', '${targetUser.user_id}')`
      ),
      client.execute(
        `insert into user_rooms (room_id, user_id) values ('${room.room_id}', '${tokenPayload.payload.user_id}')`
      )
    ])

    const [roomData] = await client.query(`
      select 
      JSON_ARRAYAGG(JSON_OBJECT(
        'room_id', r.room_id,
        'created_at', r.created_at
      )) as rooms,
      JSON_ARRAYAGG(JSON_OBJECT(
        'user_id', u.user_id,
        'email', u.email
      )) as user
      from user_rooms as ur
      left join rooms as r on ur.room_id = r.room_id
      left join users as u on u.user_id = ur.user_id
      where r.private_key = '${uuid}'
      group by ur.room_id
    `)

    roomData.room = JSON.parse(roomData.rooms)[0]
    roomData.user = JSON.parse(roomData.user)

    delete roomData.rooms

    context.response.body = {
      data: roomData,
      status: 200,
    }
  } catch (error) {
    context.response.body = {
      status: 500,
      messageError: 'Internal error',
      ...error
    }
  }
}

export const getRooms = async (context: any) => {
  try {
    const { accessToken } = context.params

    if (!accessToken) {
      throw new Error('Access token is required in the body')
    }


    const tokenPayload = await verifyJwtToken({
      token: accessToken
    })

    if (!tokenPayload || !tokenPayload.payload || !tokenPayload.payload.user_id) {
      throw new Error('Access token was invalid')
    }

    const userRooms = await client.query(`
      select *
      from user_rooms
      where user_id = '${tokenPayload.payload.user_id}'
    `)

    const roomIds = userRooms.map((each: any) => each.room_id)

    const rooms = await client.query(`
      select 
      JSON_ARRAYAGG(JSON_OBJECT(
        'room_id', r.room_id,
        'created_at', r.created_at
      )) as rooms,
      JSON_ARRAYAGG(JSON_OBJECT(
        'user_id', u.user_id,
        'email', u.email
      )) as user
      from user_rooms as ur
      left join rooms as r on ur.room_id = r.room_id
      left join users as u on u.user_id = ur.user_id
      where ur.room_id in (${roomIds.join(', ')})
      group by ur.room_id
    `)

    const roomData = rooms.map((each: any) => {
      each.room = JSON.parse(each.rooms)[0]
      each.user = JSON.parse(each.user)
      delete each.rooms

      return each
    })

    context.response.body = {
      data: roomData,
      status: 200,
    }
  } catch (error) {
    throw error
  }
}
