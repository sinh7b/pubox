// import { Application } from "https://deno.land/x/abc@v1.0.0-rc2/mod.ts";
import { Application, Router, send } from "https://deno.land/x/oak/mod.ts";
import { oakCors } from "https://deno.land/x/cors/mod.ts";
import { login, signup, validateAccessToken } from './controllers/users.controller.ts'
import { createRoom, getRooms } from './controllers/rooms.controller.ts'
import { createMessage, getMessages } from './controllers/messages.controller.ts'

// import { authentication } from './middlewares/authentication.ts'
const app = new Application();

app.use(oakCors());

// app.use((context) => {
//   // const header = context

//   console.log(context.request.url)
// })

const router = new Router();
router
  .post('/users/me/rooms',createRoom)
  .post('/auth/login', login)
  .get('/users/me/rooms/:accessToken', getRooms)
  .get('/rooms/:roomId/messages', getMessages)
  .post('/rooms/:roomId/messages', createMessage)
  .post('/auth/signup', signup)
  .post('/auth/token/verify', validateAccessToken)

app.use(router.routes());
// app.use(router.allowedMethods(['GET']));

console.log('server is on 8000')
await app.listen({ port: 8000 });

